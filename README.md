
# user-management-node
  
  

## Usage

  

``` bash

# install dependencies

$ npm install

# serve with hot reload at localhost:7001

$ npm start

```

  

## Database

Used MongoDB with Mongoose. <br />

You can change database details from **./config.js** file
