const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const config = require("./config.js");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, x-refresh-token');
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    next();
});

const userRoutes = require('./routes/User');
const groupRoutes = require('./routes/Group');
const statsRoutes = require('./routes/Stats');
const rfqRoutes = require('./routes/Rfq');
const orderRoutes = require('./routes/Order');
const tradeRoutes = require('./routes/Trade');
const assetRoutes = require('./routes/Asset');

app.use('/api/user', userRoutes);
app.use('/api/group', groupRoutes);
app.use('/api/stats', statsRoutes);
app.use('/api/rfq', rfqRoutes);
app.use('/api/order', orderRoutes);
app.use('/api/trade', tradeRoutes);
app.use('/api/asset', assetRoutes);


// Connect to Mongoose
mongoose.Promise = global.Promise;
//mongoose.connect('mongodb://' + config.db.user + ':' + config.db.password + '@' + config.db.url + '/' + config.db.database);
mongoose.connect('mongodb+srv://' + config.db.user + ':' + config.db.password + '@' + config.db.url + '/', {dbName: config.db.database});
mongoose.connection.on('connected', function () {
    console.log("MongoDB connected")
 });
 

app.get('/', function(req, res){
    res.send('Please use api endpoints to get or post data!');
});

const port = 7001;
app.listen(process.env.PORT || port);
console.log("server started on port: " + port);