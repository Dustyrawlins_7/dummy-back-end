const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const OrderSchema = new Schema({
    currency: {type: String},
    user: {type: Schema.Types.ObjectId, ref: 'User'},
    price: {type: String, required: true},
    size: {type: String, required: true},
    minq: {type: String, required: true},
    od: {type: String, required: true},
    status: {type: String, required: true}
});
const Order = module.exports = mongoose.model('Order', OrderSchema);