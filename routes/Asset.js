const express = require('express');
const router = express.Router();
const Asset = require('../models/Asset');

// Get all users list endpoint
router.post('/all', function (req, res, next) {
    Asset.find().sort({fullname: 1})
    .skip(parseInt(req.body.skip))
    .limit(parseInt(req.body.limit))
    .populate('user')
    .exec(function (err, users) {
        if(err){
            return res.status(500).json({
                code: 500,
                error: err
            })
        }else{
            res.status(200).json({
                code: 200,
                message: "success",
                data: users
            })
        }
    });
});

// Get user by id endpoint
router.post('/byid/:id', function(req, res, next){
    Asset.findById(req.params.id)
    .exec(function(err, user) {
        if(err){
            return res.status(500).json({
                code: 500,
                error: err
            })
        }else if (!user) {
            return res.status(404).json({
                code: 404,
                error: 'Asset not found'
            });
        }else{
            res.status(200).json({
                code: 200,
                data: user
            })
        }
    });
});

// Get users by user id endpoint
router.post('/byuser/:userid', function(req, res, next){
    Asset.find({user: req.params.userid})
    .populate('user')
    .exec(function(err, users) {
        if(err){
            return res.status(500).json({
                code: 500,
                error: err
            })
        }else{
            res.status(200).json({
                code: 200,
                data: users
            })
        }
    });
});

//search user endpoint
router.post('/search/:term', function (req, res, next) {
    let query;
    if(req.body.userid){
        query = {$or: [{price : { "$regex": req.params.term.trim(), "$options": "i" }}], user: req.body.userid};
    }else{
        query = {$or: [{price : { "$regex": req.params.term.trim(), "$options": "i" }}]}
    }
    Asset.find(query).limit(10)
    .populate('user')
    .exec(function(err, users) {
        if(err){
            return res.status(500).json({
                code: 500,
                error: err
            })
        }else{
            res.status(200).json({
                code: 200,
                data: users
            })
        }
    });
});

//Add user endpoint
router.post('/add', function (req, res, next) {
    if(!req.body.user){
        return res.status(400).json({
            code: 400,
            error: "Please select at least one user to add user"
        });
    }else if(!req.body.currency){
        return res.status(400).json({
            code: 400,
            error: "Please enter Currency"
        });
    }else if(!req.body.price){
        return res.status(400).json({
            code: 400,
            error: "Please enter Price"
        });
    }else if(!req.body.size){
        return res.status(400).json({
            code: 400,
            error: "Please enter Size"
        });
    }else if(!req.body.minq){
        return res.status(400).json({
            code: 400,
            error: "Please enter MinQ"
        });
    }else if(!req.body.status){
        return res.status(400).json({
            code: 400,
            error: "Please enter Status"
        });
    }else if(!req.body.od){
        return res.status(400).json({
            code: 400,
            error: "Please enter OD"
        });
    }else{
        const asset = new Asset({
            currency: req.body.currency,
            user: req.body.user,
            price: req.body.price,
            size: req.body.size,
            minq: req.body.minq,
            od: req.body.od,
            status: req.body.status
        });

        asset.save(function (err, result) {
            if (err) {
                return res.status(500).json({
                    code: 500,
                    title: 'An error occurred',
                    error: err
                });
            }else{
                res.status(200).json({
                    code:200,
                    message: 'New Asset Successfully Added',
                    type: 'created',
                    data: result
                });
            }
        });
    }

});

//Update user endpoint
router.put('/:id', function (req, res, next) {
    Asset.findById(req.params.id, function (err, asset) {
        if (err) {
            return res.status(500).json({
                code: 500,
                error: err
            });
        }else if (!asset) {
            return res.status(404).json({
                code: 404,
                error: 'Asset not found'
            });
        }else{
            asset.currency = req.body.currency;
            asset.user = req.body.user;
            asset.price = req.body.price;
            asset.size = req.body.size;
            asset.minq = req.body.minq;
            asset.od = req.body.od;
            asset.status = req.body.status;
            asset.save(function (err, result) {
                if (err) {
                    return res.status(500).json({
                        code: 500,
                        error: err
                    });
                }else{
                    res.status(200).json({
                        code:200,
                        message: 'Asset Updated'
                    });
                }
            });
        }
    });
});

//Delete user endpoint
router.delete('/:id', function(req, res, next) {
    Asset.findById(req.params.id, function (err, user) {
        if (err) {
            return res.status(500).json({
                code: 500,
                error: err
            });
        }else if (!user) {
            return res.status(404).json({
                code: 404,
                error: 'Asset not found'
            });
        }else{
            user.remove(function (err, result) {
                if (err) {
                    return res.status(500).json({
                        code: 500,
                        error: err
                    });
                }else{
                    res.status(200).json({
                        code: 200,
                        message: 'Asset deleted',
                        type: 'deleted'
                    });
                }
            });
        }
    });
});

module.exports = router;